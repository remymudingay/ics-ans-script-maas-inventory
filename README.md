# ics-ans-script-maas-inventory

A dynamic inventory script for MAAS (API version 2.0).

###
Use ansible to call this script using either the `--list` or `--host` arguments.

###
The script can also be called directly 

Note: this script will only return machines that are in "Deployed" status.

## Usage

- Set Ansible's hosts file, for example: `/etc/ansible/hosts`
- Set environment variables `API_MAAS_URL` and `API_MAAS_KEY`
